def is_number(s: str) -> bool:
    """Returns True is string is a number.

    Args:
        s (str): [description]

    Returns:
        bool: [description]
    """
    return s.replace('.', '', 1).isdigit()
