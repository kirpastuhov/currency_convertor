from http.server import BaseHTTPRequestHandler
import json
import logging

from app.get_price import fetch_price, evaluate_price

logging.basicConfig(filename="report_server.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        """Handle GET requests"""
        logger.info("server", self.server)
        logger.info("headers", self.headers)
        logger.info("GET request")
        self._set_headers()
        rub, usd = fetch_price()

        data = [
            {"USDRUB": {"USD": 1, "RUB": rub},
             "RUBUSD": {"RUB": 1, "USD": usd}}]

        self._send_response(data)

    def do_POST(self):
        """Handle POST requests"""
        try:
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            usd_amt = float(post_data)
            rub_amt = evaluate_price(usd_amt)
            data = [{"USDRUB": {"USD": usd_amt, "RUB": rub_amt}}]
            self._set_headers()
            self._send_response(data)
        except Exception as e:
            self._send_err([{"error": str(e)}])
            logger.info({"error": str(e)})

    def _set_headers(self, status_code=200):
        """Set headers for the response.

        Args:
            status_code (int, optional): Status code of the response. Defaults to 200.
        """
        self.send_response(status_code)
        self.send_header("Content-type", "application/json")
        self.end_headers()

    def _send_response(self, msg):
        """Send response

        Args:
            msg (str): Response message from server.
        """
        self.wfile.write(json.dumps(msg).encode('utf-8'))

    def _send_err(self, err):
        """Send error message

        Args:
            err (str): Error message.
        """
        self._set_headers(status_code=400)
        self.wfile.write(json.dumps(err).encode('utf-8'))


