import urllib.request
from app.html_parser import MyHTMLParser
from app.utils import is_number
import logging

logging.basicConfig(filename="report_server.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def fetch_price():
    """Make request to resource and return usd and rub prices.

    Returns:
        int: parsed rub and usd prices
    """
    req = urllib.request.Request('https://freecurrencyrates.com/en/convert-USD-RUB')
    logger.info("Request data from https://freecurrencyrates.com/en/convert-USD-RUB")
    with urllib.request.urlopen(req) as response:
        page = str(response.read())

    logger.info("Start parsing HTML from requested site")

    parser = MyHTMLParser()
    parser.feed(page)

    logger.info("Parsing done")

    data = [float(d) for d in parser.data.split() if is_number(d)]
    rub, usd = data[0], data[2]
    logger.info(f"Parsed RUB = {rub}; USD = {usd}")
    return rub, usd


def evaluate_price(usd_amt: float) -> float:
    """Evaluate amount of rub.

    Args:
        usd_amt (float): USD amount that needs to be converted into RUB

    Returns:
        float: amount of RUB
    """
    logger.info(f"Start converting {usd_amt} USD to RUB")

    rub, usd = fetch_price()
    rub_amt = usd_amt * rub

    logger.info(f"Conversion finished - {usd_amt} USD = {rub_amt} RUB")
    return rub_amt
