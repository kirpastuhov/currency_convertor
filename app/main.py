from http.server import HTTPServer
import logging

from server import MyHandler

logging.basicConfig(filename="report_server.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


def run(server_class=HTTPServer, handler_class=MyHandler, port=8080):
    server_address = ('', port)
    logger.info("Start serving")
    httpd = server_class(server_address, handler_class)
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()


if __name__ == '__main__':
    run()
