from html.parser import HTMLParser


class MyHTMLParser(HTMLParser):
    def __init__(self):
        super().__init__()
        self.data = None

    def handle_data(self, data):
        """Parse html data to find data fields.

        Args:
            data (str): string with HTML code.
        """
        if data.startswith("It means you will get"):
            self.data = data
