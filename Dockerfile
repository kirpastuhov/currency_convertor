FROM python:3.6-alpine

ENV APPDIR=/home/src
ENV PYTHONPATH=$PYTHONPATH:$APPDIR

WORKDIR $APPDIR

COPY ./app $APPDIR/app
COPY ./requirements.txt $APPDIR/

RUN pip install --upgrade --force-reinstall --no-cache-dir -r $APPDIR/requirements.txt

EXPOSE 8080

CMD /usr/local/bin/python $APPDIR/app/main.py
