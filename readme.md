Simple Currency Converter
===

## Table of Contents
* [Simple Currency Converter](#Simple Currency Converter)
     * [Description](#Description)
     * [Getting started](#Getting started)
     * [GET Request](#GET Request)
     * [POST Request](#POST Request)
     * [Tests](#Tests)
     * [Logs](#Logs)


##  Description

Implement a simple service of currency converter USD -> RUB (data source - any web resource of your choice. Not API. The data should be parsed from web page). Interface - HTTP API, which on request sends JSON with conversion result (currency, requested value, resulting value, etc.) or error with corresponding code and message in "error" section. It is obligatory to implement in "pure" python and not to use any dependencies and third-party libraries (http-server can also be implemented without dependencies).

## Getting started
Go to project directory and start the server by pasting the following line into your terminal:

``` 
docker build -t app_image . && docker run -p 8080:8080 --name app app_image
```

This will fire up the docker container with the name *app* and the server will be accessible on *localhost:8080* 

## GET Request

Simply run 
```
curl localhost:8080
```
to check if everything is all rigth. If it's true, you'll get a json in response which will show you how many RUB you will get for 1 USD or how many you can will USD for 1 RUB.
```
[{"USDRUB": {"USD": 1, "RUB": 75.9648}, "RUBUSD": {"RUB": 1, "USD": 0.0131}}]
```

## POST Request

To actually convert from USD to RUB, send POST request
```
curl -X POST -d 10  localhost:8080
```
and you'll get the converted amount in RUB.
```
[{"USDRUB": {"USD": 10.0, "RUB": 750.1569999999999}}]
```

## Tests

Ain't much is covered, but there are some.

You will need *pytest* to execute them like that
```
pytest 
```

## Logs

Most of the actions on the server are logged, and you can check that in *report_server.log* file.
