from app.get_price import fetch_price, evaluate_price


def test_fetch_price():
    usd, rub = fetch_price()

    assert isinstance(usd, float)
    assert isinstance(rub, float)


def test_eval_price():
    usd_amt = 100
    rub_amt = evaluate_price(usd_amt)

    assert isinstance(rub_amt, float)


